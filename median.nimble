# Package

version       = "0.0.1"
author        = "Artem Klevtsov"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.4.2"
