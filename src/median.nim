import median/qsort

proc median*(x: openarray[SomeNumber]): float =
  var xx = @x
  xx.quickSort()
  0.5 * float(xx[xx.high div 2] + xx[xx.len div 2])
