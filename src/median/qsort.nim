proc quickSortImpl(a: var openarray[SomeNumber], start, stop: int) =
  if stop - start > 0:
    let pivot = a[start]
    var left = start
    var right = stop
    while left <= right:
      while a[left] < pivot:
        inc(left)
      while a[right] > pivot:
        dec(right)
      if left <= right:
        swap(a[left], a[right])
        inc(left)
        dec(right)
    quickSortImpl(a, start, right)
    quickSortImpl(a, left, stop)

proc quickSort*(a: var openarray[SomeNumber]) =
  quickSortImpl(a, 0, a.len - 1)
