import unittest
import random
import algorithm
import median

randomize(1234)

suite "Test integers":
  test "Test even sorted":
    let a1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    check median(a1) == 5.5
  test "Test even reversed":
    var a1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    a1.reverse()
    check median(a1) == 5.5
  test "Test even random":
    var a1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    a1.shuffle()
    check median(a1) == 5.5
  test "Test negative even sorted":
    let a1 = [-1, -2, -3, -4, -5, -6, -7, -8, -9, -10]
    check median(a1) == -5.5
  test "Test odd sorted":
    let a2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    check median(a2) == 5.0
  test "Test odd reversed":
    var a1 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    a1.reverse()
    check median(a1) == 5.0
  test "Test odd random":
    var a2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    a2.shuffle()
    check median(a2) == 5.0
  test "Test negative odd sorted":
    let a1 = [-1, -2, -3, -4, -5, -6, -7, -8, -9]
    check median(a1) == -5.0

suite("Test floats"):
  test "Test even sorted":
    let a1 = [1.0, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    check median(a1) == 5.5
  test "Test even reversed":
    var a1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    a1.reverse()
    check median(a1) == 5.5
  test "Test even random":
    var a1 = [1.0, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    a1.shuffle()
    check median(a1) == 5.5
  test "Test odd sorted":
    let a2 = [1.0, 2, 3, 4, 5, 6, 7, 8, 9]
    check median(a2) == 5.0
  test "Test odd reversed":
    var a1 = [1.0, 2, 3, 4, 5, 6, 7, 8, 9]
    a1.reverse()
    check median(a1) == 5.0
  test "Test odd random":
    var a2 = [1.0, 2, 3, 4, 5, 6, 7, 8, 9]
    a2.shuffle()
    check median(a2) == 5.0
